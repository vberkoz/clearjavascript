/*
* Block Counter
* Declaration of variabes *
*/

var firstTextarea = document.getElementById('gettext');
var secondTexarea = document.getElementById('settext');
var colChars      = document.getElementById('count-letters');
var btnReset      = document.getElementById('reset');
var btnResetClasList = document.getElementById('reset').classList;
var tabCounter    = document.getElementById('tab-counter');
var tabGallery    = document.getElementById('tab-gallery');
var navCounter    = document.getElementById('counter');
var navGallery    = document.getElementById('gallery');

/*
* Block Counter
* Declaring functions *
*/
function copyPasteContentTextarea(first, second) { // Копируеттекст в первой Textarea и вставляет его в дугую Textarea
	second.innerHTML = first.value;
};

function countCharTextarea(textarea) { // выводит количество символов
	colChars.innerHTML = textarea.length;
};

function runCopyText() {  // Главная функция копирования текста
	if ( secondTexarea.value.length < 200) {
		copyPasteContentTextarea(firstTextarea, secondTexarea);
		countCharTextarea(secondTexarea.value);
	} else {
		btnResetClasList.add("btn-red");
		firstTextarea.disabled = 1;
	}
};

function runReset() {
		firstTextarea.value = "";
		secondTexarea.innerHTML = "";
		colChars.innerHTML = '0';
		btnResetClasList.remove("btn-red");
		firstTextarea.disabled = 0
};

/*
 * Tab
*/

var tabLinks = document.getElementsByClassName("tab-link");
var tabContent = document.getElementsByClassName("tab-content");
var activeIndex = 0;

function initClick(array) {
	for(var i =0; i < array.length; i++){
		var link = array[i];
		handleClick(link, i);
	}
};

function handleClick(link, index) {
	link.addEventListener('click', function (event) {
		event.preventDefault();
		goToTab(index);
	});
};

function goToTab(index) {
	tabLinks[activeIndex].classList.remove('is-active');
	tabContent[activeIndex].classList.remove('is-active');
	tabLinks[index].classList.add('is-active');
	tabContent[index].classList.add('is-active');
	activeIndex = index;
};

initClick(tabLinks);

/*
 * Block Counter
 * Event
*/

firstTextarea.addEventListener("keypress", runCopyText);
firstTextarea.addEventListener("keyup", runCopyText);
btnReset.addEventListener("click", runReset);

/*
 * Block Gallery
 * Declaration of variabes
*/

var objectTransferred = transformArray(data);
var btnAddGallery     = document.getElementById('add');
var elemAddImage      = document.getElementById('result');
var indexImage        = 0;
var colImage          = document.getElementById('count');
var workArr = [];

/*
 * Block Gallery
 * Declaring functions
*/

function transformArray(Arr) {       // Трансформирует объкт
	return Arr.map(function (item) {
		return {
			name: chaneCase(item.name),
			url: "http://" + item.url,
			params: gluingObject(item.params),
			id: item.id,
			description: lineCutting(item.description),
			date: formatDate(item.date)
		};
	});
};

function chaneCase(str) {
	var firstChar = str.charAt(0).toUpperCase();
	var remain = str.substring(1).toLowerCase();
	return firstChar + remain;
};

function lineCutting(str) {
	return str.substr(0, 15) + "...";
};

function gluingObject(object) {
	return object.status + " => " + object.progress;
};

function formatDate(date) {
	var tmpDate = new Date(date);
	function getCorrectDate(val) {
		if(val <10){
			return "0" + val;
		} else {return val;}
	}
	return tmpDate.getFullYear() + "/" +
		getCorrectDate(tmpDate.getMonth() + 1) + "/" +
		getCorrectDate(tmpDate.getDate()) + " " +
		getCorrectDate(tmpDate.getHours()) + ":" +
		getCorrectDate(tmpDate.getMinutes());
};

function createElementInterpolation(item, elemRow) {
	var itemTemplate = `<div class="col-sm-3 col-xs-6 ">\
				<img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
				<div class="info-wrapper">\
					<div class="text-muted">${item.name}</div>\
					<div class="text-muted">${item.description}</div>\
                    <div class="text-muted">${item.params}</div>\
					<div class="text-muted">${item.date}</div>\
				</div>\
				<button class="btn btn-danger" data-id="${item.id}">Удалить</button>\
			</div>`;

	elemRow.innerHTML += itemTemplate;
	console.log(elemRow);
};

function runAddImgGallery() {
	if(indexImage < objectTransferred.length) {
		createElementInterpolation(objectTransferred[indexImage], elemAddImage);
		indexImage++;
		colImage.innerHTML = indexImage;
	}else{
		alert("Закончились изображения");
	}
};

function removeElementFromGallery(event) {
	var target = event.target;
	var removeEement = target.getAttribute("data-id");
	elemAddImage.removeChild(target.closest('div'));
	moveElementWorkArrToSourceArr(removeEement);
	colImage.innerHTML = --indexImage;
};

function moveElementWorkArrToSourceArr(dataId) {
	objectTransferred.map(function (item, index) {
		if(item["id"] == dataId){
			var removed = objectTransferred.splice(index, 1);
			objectTransferred.push(removed[0]);
		}
	});
}
/*
 * Block Gallery
 * Event
*/

btnAddGallery.addEventListener("click", runAddImgGallery);
elemAddImage.addEventListener("click", removeElementFromGallery);