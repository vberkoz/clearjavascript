var total = 0,
    result = document.getElementById("result");

for (var i = 1; i <= 15; i++) {
  if (i == 8 || i == 13) { continue }
  
  var first = Math.floor((Math.random() * 6) + 1); 
  var second = Math.floor((Math.random() * 6) + 1);
  
  if (first == second) {
    result.innerHTML += i + ". Выпал дубль. Число " + "<b>" + first + "</b>" + "<br/>";
  } else if (first < 3 && second > 4) {
    result.innerHTML += i + ". Большой разброс между костями. Разница составляет " + "<b>" + (second - first) + "</b>" + "<br/>";
  }
  else {
    result.innerHTML += i + ". Первая кость: " + "<b>" + first + "</b>" + " Вторая кость: " + "<b>" + second + "</b>" + "<br/>";
  }
  
  total += first + second;
}

document.getElementById("total").innerHTML = total >= 100 ? "Победа, вы набрали очков " + "<b>" + total +"</b>" : "Вы проиграли, у вас очков " + "<b>" + total + "</b>"; 