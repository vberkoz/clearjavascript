﻿var btn = document.getElementById("play");
var player1 = document.getElementById("player1");
var player2 = document.getElementById("player2");
var result = document.getElementById("result");
var getPlayerResult = () => {
    return Math.floor((Math.random() * 3) + 1);
}
var getNameById = (name) => {
    switch (name) {
        case 1:
            name = 'камень';
            break;
        case 2:
            name = 'ножницы';
            break;
        case 3:
            name = 'бумага';
            break;
    }
    return name;
}
var determineWinner = (first, second) => {
	var winner;
	first == second ? winner = 'Ничья, играем еще раз' : 
	(first == 1) && (second == 2) ? winner = 'Выиграл Первый игрок' : 
	(first == 3) && (second == 1) ? winner = 'Выиграл Первый игрок' :
	(first == 2) && (second == 3) ? winner = 'Выиграл Первый игрок' :
	winner = 'Выиграл Второй игрок';
		return winner;
}
var printResult = (result, text) => {
	result.innerHTML = text;
}
var runGame = () => {
	var firstHand = getPlayerResult();
	var secondHand = getPlayerResult();
	player1.innerHTML = getNameById(firstHand);
    player2.innerHTML = getNameById(secondHand);
	var text = determineWinner(firstHand, secondHand);
	printResult (result, text);
}
btn.addEventListener("click", runGame);
