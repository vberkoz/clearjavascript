"use srtrict";
var countDiv = document.querySelector('.counter');
var galleryDiv = document.querySelector('.gallery');
var displayCounter = () => {
	countDiv.style.display = 'block';
	galleryDiv.style.display = 'none';
};
var displayGallery = () =>{
	galleryDiv.style.display = 'block';
	countDiv.style.display = 'none';
};	
var btnReset = document.querySelector('#reset');
var textEnter = document.querySelector('#gettext');
var textDisplay = document.querySelector('#settext');
var counter = document.querySelector('#count-letters');
textEnter.addEventListener('keyup', function () {
	textDisplay.value = textEnter.value;
	counter.innerHTML = textEnter.value.length;
	if (textEnter.value.length >= 200){
		btnReset.style.backgroundColor = 'red';
		textEnter.disabled = true;
	}
	else{
		btnReset.style.backgroundColor = '';
	}
});
var clearData = () => {
	textEnter.value = '';
	textDisplay.value = '';
	counter.innerHTML = '0';
	btnReset.style.backgroundColor = '';
	textEnter.disabled = false;
};
var resultDiv = document.querySelector('#result');
var btnAddPicture = document.querySelector('#add');
var picsNumber = 0;
var picsCount = document.querySelector('#count');
var btnCounter = document.querySelector('#counter');
var btnGallery = document.querySelector('#gallery');
var modifyName = (name) => {
	return name.charAt(0).toUpperCase() + name.substr(1).toLowerCase();
};
var modifyUrl = (url) => {
	return 'http://' + url; 
};
var modifyDescript = (descript) => {
	return descript.substr(0, 15) + '...';
}; 
var modifyDate = (date) => {
        var tmpDate = new Date(date);
        var fixedDate = {
			year:    tmpDate.getFullYear(),
            month:   tmpDate.getMonth()+1,
            day:     tmpDate.getDate(),
            hours:   tmpDate.getHours(), 
            minutes: tmpDate.getMinutes(),
		};
		for (var key in fixedDate){
			fixedDate[key] = fixedDate[key] < 10 ? '0' + fixedDate[key] : fixedDate[key];
		}
		return fixedDate.year + '/' + fixedDate.month + '/' + fixedDate.day + ' ' + fixedDate.hours + ':' + fixedDate.minutes;
};
var modifyParams = (params) => {
	return params.status + ' => ' + params.progress;
};
var modifyData = (newData) => {
	return newData.map(function(item,index){
      return{
		name: modifyName(item.name),
        url: modifyUrl(item.url),
		description: modifyDescript(item.description),
		date: modifyDate(item.date),
		params: modifyParams(item.params)
      };
	});
};
var applyInterpolation = (item, displayDiv) => {
var itemTemplate = `<div class="col-md-3 col-sm-4 col-xs-6 text-center">\
				<div class="thumbnail">\
				<img alt="${item.name}" src="${item.url}"></img>\
					<div class="caption">\
						<h3 class="text-muted">${(picsNumber + 1) + ": " + item.name}</h3>\
						<p>${item.description}</p>\
						<p>${item.params}</p>\
						<p>${item.date}</p>\
					</div>\
					<button class="btn btn-danger" onclick="deleteEl()">${"Удалить"}</button>\
				</div>\
			</div>`; 
displayDiv.innerHTML += itemTemplate;
};
var modifiedData = modifyData(data);
var displayPics = () => {
	if (picsNumber < modifiedData.length){
		applyInterpolation(modifiedData[picsNumber], resultDiv);
		picsNumber++;
		picsCount.innerHTML = picsNumber;
	}
	else{
		alert('Больше нет фотографий')
	}
}
(function initializeEvents () {
	btnCounter.addEventListener('click', displayCounter);
	btnGallery.addEventListener('click', displayGallery);
	window.onload = clearData;
	btnReset.addEventListener('click', clearData);
	btnAddPicture.addEventListener('click', displayPics);
})()