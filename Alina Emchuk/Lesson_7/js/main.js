function capitalizeFirstLetter(word){
	return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase();
}
function addProtocol(adress){
	return "http://" + adress;
}
function truncateString(string){
	return string.substring(0, 15) + "...";
}
function formatDate(date){
	var tmpDate = new Date(date);
	var convertedDate = {
		year: tmpDate.getFullYear(),
		month: tmpDate.getMonth() + 1,
		date: tmpDate.getDate(),
		hours: tmpDate.getHours(),
		minutes: tmpDate.getMinutes()
	}
	for (var key in convertedDate){
		convertedDate[key] = (convertedDate[key] < 10) ? "0" + convertedDate[key] : convertedDate[key];
	}
	return convertedDate.year + "/" + convertedDate.month + "/" + convertedDate.date + " " + convertedDate.hours + ":" +
			convertedDate.minutes;
}
function editParams(firstPar, secondPar){
	return String(firstPar) + " => " + String(secondPar);
}
function transformeArr(arr){
	return arr.map(function(item){
		return {
			name: capitalizeFirstLetter(item.name),
			url: addProtocol(item.url),
			description: truncateString(item.description),
			date: formatDate(item.date),
			params: editParams(item.params.status, item.params.progress)
		}
	});
}

function printWithReplace (item, row){
	var resultHTML = "";
	var itemTemplate = '<div class="col-sm-3 col-xs-6">\
				<img src="$url" alt="$name" class="img-thumbnail">\
				<div class="info-wrapper">\
					<div class="text-muted">$name</div>\
					<div class="text-muted">$description</div>\
                    <div class="text-muted">$params</div>\
					<div class="text-muted">$date</div>\
				</div>\
			</div>';
	resultHTML = itemTemplate
			.replace(/\$name/gi, item.name)
			.replace("$url", item.url)
			.replace("$params", item.params)
			.replace("$description", item.description)
			.replace("$date", item.date);

	row.innerHTML += resultHTML;
}
function printWithInterpolation(item, row){
	var resultHTML = `<div class="col-sm-3 col-xs-6">\
		<img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
		<div class="info-wrapper">\
			<div class="text-muted">${item.name}</div>\
			<div class="text-muted">${item.description}</div>\
			<div class="text-muted">${item.params}</div>\
			<div class="text-muted">${item.date}</div>\
		</div>\
		</div>`;
	row.innerHTML += resultHTML;
}
function printWithCreate(item, row){
	var newDiv = document.createElement('div');
	newDiv.className = "col-sm-3 col-xs-6";
	row.appendChild(newDiv);

	var newImg = document.createElement('img');
	newImg.src = item.url;
	newImg.alt = item.name;
	newImg.className = "img-thumbnail";
	newDiv.appendChild(newImg);

	var newDivChild = document.createElement('div');
	newDivChild.className = "info-wrapper";
	newDiv.appendChild(newDivChild);

	var textMutedDivs = [item.name, item.description, item.params, item.date];
	textMutedDivs.forEach(function(el){
		var div = document.createElement('div');
		div.className = "text-muted";
		div.innerHTML = el;
		newDivChild.appendChild(div);
	})
}

var editData = transformeArr(data);

function runGallery (array){
	var first = document.querySelector('#first-line');
	var second = document.querySelector('#second-line');
	var third = document.querySelector('#third-line');
	array.forEach(function(item, index){
		if (index < 3) {
			printWithReplace(item, first);
		} else if (index < 6) {
			printWithInterpolation(item, second);
		} else if (index < 9){
			printWithCreate(item, third);
		}
	});
}
runGallery (editData);



