(function(){

	var visibleItemsArray = [],
		data = [],
		saveGallery,
		successNotice,
		galleryArea,
		btnAddPhoto,
		btnRemovePhoto,
		countPhoto;

	function listenButtons() {
		saveGallery	= document.querySelector('#save-gallery');
		successNotice = document.querySelector(".bg-success");
		galleryArea = document.querySelector('#gallery-area');
		btnAddPhoto = document.getElementById('add-photo');
		btnRemovePhoto = document.getElementById('gallery-area');
		countPhoto = document.getElementById('count-photo');
		saveGallery.addEventListener('click', sendToServer);
		btnAddPhoto.addEventListener('click', addPhoto);
		btnRemovePhoto.addEventListener('click', removePhoto)
	}

	function showSuccessMsg(){
		successNotice.classList.remove('hidden');
	}

	function sendToServer() {
		$.post( "api/save", {visibleItemsArray}, function( data ) {
			if(data == 'ok') {
				showSuccessMsg();
			}
		})
	}

	function convertData(incomingArray) {
		function newUrl(url) {return "http://" + url;}
		function newName(name) {return name[0].toLocaleUpperCase() + name.substring(1).toLowerCase();}
		function newParams(params) {return params.status + "=>" + params.progress;}
		function newDescription(description) {return description.substring(0, 15) + "...";}
		function fixDateString(start) {return (start < 10) ? "0" + start : start;}
		function newDate(dat){
			var tmpDate = new Date(dat);
			return fixDateString(tmpDate.getFullYear()) + "/" +
			fixDateString(tmpDate.getMonth() + 1) + "/" +
			fixDateString(tmpDate.getDate()) + " " +
			fixDateString(tmpDate.getHours()) + ":" +
			fixDateString(tmpDate.getMinutes());
		}

		incomingArray.map(function (item, index) {
			item.url = newUrl(item.url);
			item.name = newName(item.name);
			item.date = newDate(item.date);
			item.params = newParams(item.params);
			item.description = newDescription(item.description);
		})
		data = incomingArray;
	}

	function createGalleryItem(item, domElement) {
		var itemTemplate = `<div class="col-sm-3 col-xs-3">\
								<img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
								<div class="info-wrapper">\
									<div class="text-muted">${item.name}</div>\
									<div class="text-muted">${item.description}</div>\
									<div class="text-muted">${item.params}</div>\
									<div class="text-muted">${item.date}</div>\
								</div>\
								<button class="btn btn-danger" type="button" id="${item.id}">Удалить</button>\
							</div>`;
		domElement.innerHTML += itemTemplate;
	}

	function addPhoto() {
		if (data.length > 0) {
			createGalleryItem(data[0], galleryArea);
			visibleItemsArray.unshift(data.shift());
			displayAvalPhoto();
		}
	}

	function removePhoto(event) {
		var target = event.target;
		if (target.className == 'btn btn-danger') {
			function findElement(item, index) {
				if (item.id == target.id) {
					data.push(visibleItemsArray[index]);
					visibleItemsArray.splice(index, 1);
				}
			}
			visibleItemsArray.map(findElement); 
			var child = target.parentNode;
			child.parentNode.removeChild(child);
			displayAvalPhoto();
		}
	}

	function displayAvalPhoto() {
		countPhoto.innerHTML = data.length;
	}

	$.getJSON( "js/data1.json", convertData);
	listenButtons();
	displayAvalPhoto();

}())