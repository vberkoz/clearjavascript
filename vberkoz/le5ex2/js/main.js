var btn = document.getElementById("play");
var player1 = document.getElementById("player1");
var player2 = document.getElementById("player2");
var result = document.getElementById("result");

function getPlayerResult() {
    return Math.floor((Math.random() * 3) + 1);
}


function runGame() {
    
    var pl1 = getPlayerResult(),
        pl2 = getPlayerResult();
    player1.innerHTML = getNameById(pl1);
    player2.innerHTML = getNameById(pl2);
    printResult(determineWinner(pl1, pl2));
}


function printResult(winner) {
    switch (winner) {
    case 0:
        result.innerHTML = "ничья";
        break;
    case 1:
        result.innerHTML = "выиграл первый игрок";
        break;
    case 2:
        result.innerHTML = "выиграл второй игрок";
        break;
    }
}


function determineWinner(firstPlayer, secondPlayer) {
    if (firstPlayer === secondPlayer) {return 0; }
    switch (firstPlayer) {
    case 1:
        switch (secondPlayer) {
        case 2: return 1;
        case 3: return 2;
        }
    case 2:
        switch (secondPlayer) {
        case 1: return 2;
        case 3: return 1;
        }
    case 3:
        switch (secondPlayer) {
        case 1: return 1;
        case 2: return 2;
        }
    }
}


function getNameById(id) {
    
    switch (id) {
    case 1:
        return "камень";
    case 2:
        return "ножницы";
    case 3:
        return "бумага";
    }
    
}

btn.addEventListener("click", runGame);