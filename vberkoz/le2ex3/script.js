//Lesson 2, exercise 3 homework
//
//Declare a variable, but do not assign a value to it.
//Output it to the console to make sure that it is undefined.
//Asign boolean value and output to the console. Do that with number, string and null values.
//Check the variable with null using the typeof operator.

var var1;

console.log(var1);

var1 = false;

console.log(var1);

var1 = 0;

console.log(var1);

var1 = "string";

console.log(var1);

var1 = null;

console.log(var1);

console.log(typeof (var1));