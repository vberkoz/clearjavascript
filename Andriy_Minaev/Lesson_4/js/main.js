    var total = 0;
    var resultId = document.getElementById("result"); 
for (var i = 1; i <= 15; i++){
   if (i == 8 || i == 13){
        continue;
    }
    var first = Math.floor((Math.random() * 6) + 1);
    var second = Math.floor((Math.random() * 6) + 1);
    
	resultId.innerHTML += "Первая кость: " + first + " Вторая кость: " + second + "<br>";
    
    if (first === second){
        var number = first;

        resultId.innerHTML += "Выпал дубль. Число " + number + "<br>";
    }
    
    if (first < 3 && second > 4){
        var diff = second - first;
        resultId.innerHTML += "Большой разброс между костями. Разница составляет " + diff + "<br>";
    }
  
    total += first + second; 
}

resultId.innerHTML += (total > 100) ? ("Победа, вы набрали " + total + " очков" + "<br>"):("Вы проиграли, у вас " + total + " очков" + "<br>")
