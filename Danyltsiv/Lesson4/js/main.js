var total = 0;
var myResult = document.getElementById("result");
for(var i = 1; i <= 15; i++){
	if (i == 8 || i == 13) {continue};
		var first = Math.floor((Math.random() * 6) + 1);
		var second = Math.floor((Math.random() * 6) +1);
	myResult.innerHTML += "<div class='resultdice'>" + "Кинули кубіки " + i + " раз" + " " + "<div class='firstdice'>" + " " +"<div class='dice" + first + "'>" + "</div>" + "<div class='dice" + second + "'>" + "</div>" + "</div>";
	if (first === second){
		var number = first;
		myResult.innerHTML += "<div class='doubleresultdice'>" + "Випав дубль. Число " + number + "</div>";
	}
	if (first < 3 && second > 4){
		var difference = (first < 3) ? second - first : first - second;
		myResult.innerHTML += "<div class='differenceindice'>" + "Великий разброс між костями. Разниця становить: " + difference + "</div>";
	}
	total += first + second;
};
myResult.innerHTML += (total > 100) ? "<div class='resulttotaldice'>" + "<span class='windice'>" + "Перемога, ви набрали очків " + "<b>" + total + "</b>" + "</span>" + "</div>" : "<div class='resulttotaldice'>" + "<span class='lostdice'>" +"Ви проиграли, у вас очків " + "<b>" + total + "</b>" + "</span>" +"</div>";